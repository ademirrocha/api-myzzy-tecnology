<?php

namespace App\Models\Api;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
     use Notifiable;
}
