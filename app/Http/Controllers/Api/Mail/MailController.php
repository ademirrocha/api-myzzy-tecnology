<?php

namespace App\Http\Controllers\Api\Mail;

use Illuminate\Notifications\Notifiable;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Notifications\SendEmail;
use App\Models\Api\Mail;

class MailController extends Controller
{
    use Notifiable;

    protected function updateDotEnv($key, $newValue, $delim=''){

	    $path = base_path('.env');
	    // get old value from current env
	    $oldValue = env($key);

	    // was there any change?
	    if ($oldValue === $newValue) {
	        return;
	    }

	    // rewrite file content with changed data
	    if (file_exists($path)) {
	        // replace current value with new value 
	        file_put_contents(
	            $path, str_replace(
	                $key.'='.$delim.$oldValue.$delim, 
	                $key.'='.$delim.$newValue.$delim, 
	                file_get_contents($path)
	            )
	        );
	    }
	}

	public function sendEmailGet(){
		return ('Não foi possível enviar seu E-mail! Erro ao enviar sua requisição... Para mais informações contate a equipe de suporte em https://myzzy.com.br...');
	}

    public function minhaChave($get){

        $user = Mail::where('email', $get)
                        ->get();

        if( $user->count() > 0 ){
            return ("Sua Chave de Acesso: ".$user[0]->token_smtp);
        }else{

            return ('Não foi possível encontrar sua chave! Verifique se seus dados estão corretos e tente novamente...Para mais informações contate a equipe de suporte em https://myzzy.com.br...');
        }
    }



    public function sendEmail(Request $request){


    	if( ! $request->you_token || ! $request->name || ! $request->email || ! $request->assunto || ! $request->descricao ){

    		return ('Não foi possível enviar seu E-mail! Requisição faltando dados! Para mais informações contate a equipe de suporte em https://myzzy.com.br...');
    	}

    	$user = Mail::where('token_smtp', $request->you_token)->get();

    	if( $user->count() == 0 ){
    		return ('Não foi possível enviar seu E-mail! Origem de envio desconhecido! Para mais informações contate a equipe de suporte em https://myzzy.com.br...');
    	}



    	//$this->updateDotEnv('APP_NAME', kebab_case($user[0]->name));
    	//$this->updateDotEnv('APP_URL', $user[0]->url_site);
    	//$this->updateDotEnv('MAIL_HOST', $user[0]->MAIL_HOST);
    	//$this->updateDotEnv('MAIL_PORT', $user[0]->MAIL_PORT);
    	//$this->updateDotEnv('MAIL_USERNAME', $user[0]->MAIL_USERNAME);
    	//$this->updateDotEnv('MAIL_PASSWORD', $user[0]->MAIL_PASSWORD);
    	//$this->updateDotEnv('MAIL_ENCRYPTION', $user[0]->MAIL_ENCRYPTION);

    	//($nameFrom, $nameTo, $to, $from, $subject, $text, $urlSite)

    	$nameFrom = $request->name ?? $request->nome;
        $fone = $request->fone ?? false;
    	$nameTo = $user[0]->name;
    	$to = $user[0]->email;
    	$from = $request->email;
    	
    	$subject = $request->assunto;
    	$text = $request->descricao;
    	$urlSite = $user[0]->url_site;

    	

    	$user[0]->notify(new SendEmail($nameFrom, $nameTo, $from, $to, $subject, $text, $urlSite, $fone));

    	return ("Seu E-mail foi enviado com sucesso!");
    }

}
