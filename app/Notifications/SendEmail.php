<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;



use Illuminate\Support\Facades\Mail;

class SendEmail extends Notification
{
    use Queueable;

    private $nameFrom;
    private $fone;
    private $nameTo;
    private $to;
    private $from;
    private $subject;
    private $text;
    private $urlSite;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($nameFrom, $nameTo, $from, $to, $subject, $text, $urlSite, $fone)
    {
        $this->nameFrom = $nameFrom;
        $this->fone = $fone;
        $this->nameTo = $nameTo;
        $this->to = $to;
        $this->from = $from;
         $this->subject = $subject;
        $this->text = $text;
        $this->urlSite = $urlSite;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable){

        $contato = 'Telefone para contato: '.$this->fone;

            if(!$this->fone)
                $contato = '';

                return (new MailMessage)
                    ->from($this->from, $this->nameFrom)
                    ->greeting('Olá ' . $this->nameTo.'! Este é um e-mail enviado por '.$this->nameFrom.'...')
                    ->replyTo($this->from, $this->nameFrom)
                    ->subject($this->subject)
                    ->line($this->text)
                    ->line($contato)
                    ->action('Enviar um e-mail para '.$this->nameFrom, url("mailto:$this->from"))
                    ->salutation("By Myzzy Tecnologia - www.myzzy.com.br");
        

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
