<?php

use Illuminate\Database\Seeder;
use App\Models\Api\Mail;

class MailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mail = Mail::get();


        //create conta space
        if($mail->count() < 1){
 
            Mail::create([
                'id' => 1,
            	'name' => 'Space Ambientes Planejados',
            	'email' => 'spaceambientesplanejados@gmail.com',
            	'MAIL_HOST' => 'smtp.gmail.com',
            	'MAIL_PORT' => '587',
            	'MAIL_USERNAME' => 'spaceambientesplanejados@gmail.com',
            	'MAIL_PASSWORD' => 'myzzy123',
            	'MAIL_ENCRYPTION' => 'tls',
            	'token_smtp' => bcrypt('spaceambientesplanejados_smtp_myzzy'),
            	'url_site' => 'http://www.spaceeambientesplanejados.com.br',  
            ]);
        }

        //create conta Myzzy
        if($mail->count() < 2){

            Mail::create([
                'id' => 2,
                'name' => 'Myzzy Tecnologia',
                'email' => 'myzzy.contact@gmail.com',
                'MAIL_HOST' => 'smtp.gmail.com',
                'MAIL_PORT' => '587',
                'MAIL_USERNAME' => 'myzzy.contact@gmail.com',
                'MAIL_PASSWORD' => 'MyzzyContacts123',
                'MAIL_ENCRYPTION' => 'tls',
                'token_smtp' => bcrypt('myzzy_tecnologia_smtp_myzzy'),
                'url_site' => 'http://www.myzzy.com.br', 
            ]);
        }


         //create conta MoriaLajes
        if($mail->count() < 3){

            Mail::create([
                'id' => 3,
                'name' => 'Moria Lajes',
                'email' => 'noreply.moria.lajes@gmail.com',
                'MAIL_HOST' => 'smtp.gmail.com',
                'MAIL_PORT' => '587',
                'MAIL_USERNAME' => 'noreply.moria.lajes@gmail.com',
                'MAIL_PASSWORD' => 'Myzzy123',
                'MAIL_ENCRYPTION' => 'tls',
                'token_smtp' => bcrypt('moria_lajes_smtp_myzzy'),
                'url_site' => 'https://morialajes.com.br/', 
            ]);
        }



         //create conta Tec Telecom
        if($mail->count() < 4){

            Mail::create([
                'id' => 4,
                'name' => 'Tec Telecom - Soluções',
                'email' => 'tiademir.rocha93@gmail.com',
                'MAIL_HOST' => 'smtp.gmail.com',
                'MAIL_PORT' => '587',
                'MAIL_USERNAME' => 'tecfke@gmail.com',
                'MAIL_PASSWORD' => 'Myzzy1237687898',
                'MAIL_ENCRYPTION' => 'tls',
                'token_smtp' => bcrypt('Tec_Telecom_Soluções_smtp_myzzy'),
                'url_site' => 'https://tectelecomsolucao.com.br/', 
            ]);
        }

    }
}
