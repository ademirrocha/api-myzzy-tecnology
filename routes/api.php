<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['cors'], 'namespace' => 'Api\Mail' , 'prefix' => 'api' ], function(){
	
	Route::get('send-mail', 'MailController@sendEmailGet')->name('send-mail');

	Route::get('minha-chave-de-acesso/{get}/', 'MailController@minhaChave')->name('minha-chave-de-acesso/{get}/');
	
	Route::POST('send-mail/api', 'MailController@sendEmail')->name('send-mail/api');

});






Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
