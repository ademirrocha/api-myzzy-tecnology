<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*

Route::group(['middleware', ['auth'], 'namespace' => 'Lotofacil' , 'prefix' => 'lotofacil' ], function(){
	

	Route::get('cadastrar-concurso', 'CadastroController@showCadastroConcurso')->name('cadastrar-concurso');
	
	Route::POST('cadastrar-concurso', 'CadastroController@saveCadastroConcurso')->name('cadastrar-concurso');


});
*/

Route::group(['middleware' => ['cors'], 'namespace' => 'Api\Mail' , 'prefix' => 'api' ], function(){
	

	Route::get('send-mail', 'MailController@sendEmailGet')->name('send-mail');

	Route::get('minha-chave-de-acesso/{get}/', 'MailController@minhaChave')->name('minha-chave-de-acesso/{get}/');
	
	Route::POST('send-mail', 'MailController@sendEmail')->name('send-mail');


});


Route::get('config', 'Controller@config')->name('config');



Route::get('/', function () {
    return view('welcome');
});
